> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Lindsey Mabrey

### Project 2 Requirements:

*Sub-Heading:*

1. MVC framework, using basic client and server-side validation
2. JSTL to prevent XSS
3. Completes CRUD functionality

#### README.md file should include the following items:

* Web application screen shots


#### Project 2 Screenshots:

*Screenshot of valid user form entry*:
![User form entry](img/ValidUserEntry.png)
*Screenshot of passed validation*:
![Form validation](img/PassedValidation.png)
*Screenshot of display of data*:
![Customer data displayed](img/DisplayData.png)
*Screenshot of the modify form*:
![Modify form](img/ModifyForm.png)
*Screenshot of the modified data*:
![Modified form](img/ModifiedData.png)
*Screenshot of delete warning*:
![Delete warning](img/DeleteWarning.png)
*Database changes- highlighted cus_id 1= updated user, highlighted cus_id 15 = deleted user, highlighted cus_id 16= added user*:
![Database changes](img/DatabaseChanges.png)

