<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Lindsey Mabrey">
	<link rel="icon" href="favicon.ico">

	<title>Lindsey Mabrey's Online Portfolio</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">

<!-- Custom styles with this template -->
<link href="css/starter-template.css" rel="stylesheet">

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<link rel="stylesheet" type=""text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css/>
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/2.0.2/css/responsive.dataTables.min.css"/>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

<!-- Carousel styles -->
<style type="text/css">
h2
{
	margin: 0;     
	color: #666;
	padding-top: 90px;
	font-size: 52px;
	font-family: "trebuchet ms", sans-serif;    
}
.item
{
	background: #333;    
	text-align: center;
	height: 300px !important;
}
.carousel
{
  margin: 20px 0px 20px 0px;
}
.bs-example
{
  margin: 20px;
}
</style>

</head>
<body>

	<%@ include file="global/nav_global.jsp" %>
	
	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
							<%@ include file="global/header.jsp" %>
						</div>

<!-- Start Bootstrap Carousel  -->
<div class="bs-example">
	<div
      id="myCarousel"
		class="carousel"
		data-interval="1000"
		data-pause="hover"
		data-wrap="true"
		data-keyboard="true"			
		data-ride="carousel">
		
    	<!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            
        </ol>   
       <!-- Carousel items -->
        <div class="carousel-inner">

          <div class="active item">
                <h2>About Me</h2>
                <div class="carousel-caption">
                  <h3>About Me:</h3>
                  <p>I am a Junior at Florida State University. I am in the College of Communication and Information, majoring in Information Technology. After graduation in May 2017, I plan to get my Masters focusing on leadership and  management. My dream job is to be an IT project manager or specialist.</p>
                </div>
            </div>

            <div class="item">
                <h2>Academics</h2>
                <div class="carousel-caption">
                  <h3>Academics:</h3>
                  <p>With my focus in IT, I have taken the following courses: 

                  	Foundations of Management Inforformation Systems, Technical Communication for Information Professionals, Research and Data Analytics for Information Professionals, Information Science, Information Technologies, Information Organization and Communication, Problem Solving with Object Oriented Programming, Web Applications Development, Advanced Web Applications Development, Database Concepts, and Digital Graphic Design.</p>
						 									
                </div>
            </div>

            

        </div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
<!-- End Bootstrap Carousel  -->

<!-- Placeholder for responsive table if needed.  -->
 <div class="table-responsive">
	 <table id="myTable" class="table table-striped table-condensed" >

		 <!-- Code displaying table data with Edit/Delete buttons goes here // -->

	 </table>
 </div> <!-- end table-responsive -->

 	<%@ include file="global/footer.jsp" %>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

    <!-- Bootstrap core JavaScript: jQuery necessary for Bootstrap's JavaScript plugins
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.0.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/2.0.2/js/dataTables.responsive.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>

	<script>
	 $(document).ready(function(){
    $('#myTable').DataTable();
});
	</script>

	
</body>
</html>
